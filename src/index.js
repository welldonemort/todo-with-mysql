const express = require('express');
const bodyParser = require('body-parser');
const initializePassport = require('./lib/auth');
const { connectToMySQL } = require('./db/index');

const app = express();
const routes = require('./routes');
const config = require('./config');

const PORT = 3000;

// SQL
const mysql = connectToMySQL(config);
config.mysql.client = mysql;
const { initialize } = initializePassport(mysql);

// middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(initialize);

app.use('/api', routes(config));

app.use((err, req, res, next) => {
  console.error(err);
  const { message } = err;
  const status = err.status || 500;
  return res.json({ status, message });
});

app.listen(PORT, () => console.log(`Express server listening on port http://localhost:${PORT}!`));

const successCallback = (res, body) => res.json(body);
const errorCallback = (res, error) => {
  console.error(error);
  return res.status(500).json({ message: error.message });
};

module.exports = {
  successCallback,
  errorCallback,
};

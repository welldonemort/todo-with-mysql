const { DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');

// кол-во раундов хэширования
const SALT_ROUNDS = 12;

module.exports = (sequelize) => {
  const Task = sequelize.define('Task', {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: [3, 255],
      },
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [8, 255],
      },
    },
    done: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  });

  const User = sequelize.define(
    'User',
    {
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: [6, 255],
        },
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: [3, 255],
        },
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: [3, 255],
        },
      },
    },
    {
      timestamps: true,
      hooks: {
        beforeCreate: async (user) => {
          if (user.password) {
            const salt = await bcrypt.genSalt(SALT_ROUNDS); // Generate salt
            user.password = await bcrypt.hash(user.password, salt); // Hash password
          }
        },
      },
    }
  );

  User.hasMany(Task);
  Task.belongsTo(User, {
    onDelete: 'CASCADE',
    foreignKey: {
      allowNull: false,
    },
  });

  sequelize.sync();

  return {
    User,
    Task,
  };
};

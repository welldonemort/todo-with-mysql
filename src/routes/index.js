const express = require('express');

const router = express.Router();

const tasksRoute = require('./tasks/tasks');
const authRoute = require('./auth/auth');

module.exports = (config) => {
  router.use('/tasks', tasksRoute(config));
  router.use('/auth', authRoute(config));

  return router;
};

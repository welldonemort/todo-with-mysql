const express = require('express');
const initializePassport = require('../../lib/auth');
const { successCallback, errorCallback } = require('../../helpers/withAsync');
const TasksService = require('../../services/TasksService');

const router = express.Router();

module.exports = (config) => {
  const tasksService = new TasksService(config.mysql.client);
  const { authenticateJwt } = initializePassport(config.mysql.client);

  // 3
  router.post('/', authenticateJwt, async (req, res) => {
    try {
      await tasksService.inTransaction(async (transaction) => {
        const { body } = req;
        const userId = req.user.id;
        const params = { ...body, done: false, userId };

        await tasksService.createTask(params, transaction);
      });

      successCallback(res, { message: 'Task was successfully added!' });
    } catch (error) {
      errorCallback(res, error);
    }
  });

  // 4
  router.get('/', authenticateJwt, async (req, res) => {
    try {
      const userId = req.user.id;
      const tasks = await tasksService.getUserTasks(userId);
      return successCallback(res, { tasks });
    } catch (error) {
      return errorCallback(res, error);
    }
  });

  // 5
  router.post('/done', authenticateJwt, async (req, res) => {
    try {
      const { title } = req.body;
      const userId = req.user.id;

      if (!title) {
        return res.status(400).json({ message: 'Invalid title!' });
      }

      const task = await tasksService.getTaskByTitle(title);

      if (!task) {
        return res.status(404).json({ message: 'Task not found' });
      }
      if (task.done) {
        return res.status(400).json({ message: 'This task is already marked as "done"!' });
      }

      if (task.UserId.toString() !== userId.toString()) {
        return res.status(403).json({ message: "Sorry, it's not your task!" });
      }

      await tasksService.inTransaction(async (transaction) => {
        await tasksService.updateTaskByTitle(title, { done: true }, transaction);
      });

      return successCallback(res, { message: 'Task was updated successfully!' });
    } catch (error) {
      return errorCallback(res, error);
    }
  });

  // 6
  router.get('/done', authenticateJwt, async (req, res) => {
    try {
      const userId = req.user.id;
      const tasks = await tasksService.getUserTasks(userId, true);
      return successCallback(res, { tasks });
    } catch (error) {
      return errorCallback(res, error);
    }
  });

  // 7
  router.put('/:taskTitle', authenticateJwt, async (req, res) => {
    try {
      const { taskTitle } = req.params;
      const userId = req.user.id;
      const params = req.body;

      let task;

      await tasksService.inTransaction(async (transaction) => {
        task = await tasksService.updateTaskByTitle(taskTitle, params, transaction);
      });

      if (!task) {
        return res.status(404).json({ message: 'Task not found' });
      }

      if (task.UserId.toString() !== userId.toString()) {
        return res.status(403).json({ message: "Sorry, it's not your task!" });
      }

      return successCallback(res, { message: 'Task was updated successfully!', task });
    } catch (error) {
      return errorCallback(res, error);
    }
  });

  // 8
  router.delete('/', authenticateJwt, async (req, res) => {
    try {
      const { title } = req.body;
      const deletedTask = await tasksService.deleteTaskByTitle(title);
      if (!deletedTask) {
        return res.status(404).json({ message: 'Task not found' });
      }
      return successCallback(res, { message: 'Task was deleted successfully!' });
    } catch (error) {
      return errorCallback(res, error);
    }
  });

  return router;
};

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const getModels = require('../models/sequelize');

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'your-secret-key',
};

const initializePassport = (sequelize) => {
  const { User } = getModels(sequelize);

  passport.use(
    new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
      try {
        const user = await User.findByPk(jwtPayload.userId);
        if (!user) {
          return done(null, false, { message: 'User not found' });
        }
        return done(null, user);
      } catch (err) {
        return done(err);
      }
    })
  );

  const authenticateJwt = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user, info) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({ message: 'Please login to make this request!' });
      }
      req.user = user;
      return next();
    })(req, res, next);
  };

  return {
    initialize: passport.initialize(),
    authenticateJwt,
  };
};

module.exports = initializePassport;

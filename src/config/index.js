module.exports = {
  mysql: {
    options: {
      host: 'localhost',
      port: 3406,
      database: 'todo',
      dialect: 'mysql',
      username: 'root',
      password: 'mypassword',
    },
    client: null,
  },
};

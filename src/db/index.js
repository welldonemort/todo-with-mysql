/* eslint-disable no-console */
const Sequelize = require('sequelize');

function connectToMySQL(config) {
  const sequelize = new Sequelize(config.mysql.options);
  sequelize
    .authenticate()
    .then(() => {
      console.info('Successfully connected to MySQL');
    })
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
  return sequelize;
}

module.exports = { connectToMySQL };

/* eslint-disable class-methods-use-this */
const jwt = require('jsonwebtoken');
const Models = require('../models/sequelize');
const bcrypt = require('bcrypt');

class UsersService {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  async inTransaction(work) {
    const t = await this.client.transaction();
    try {
      await work(t);
      return t.commit();
    } catch (error) {
      t.rollback();
      throw error;
    }
  }

  /**
   * Login user
   * @param {string} email User email
   * @param {string} password User password
   */
  async login(payload) {
    const { email, password } = payload;

    const user = await this.models.User.findOne({ where: { email } });

    if (!user) {
      throw new Error('Invalid credentials');
    }
    if (!password) {
      throw new Error('Password is required!');
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      throw new Error('Invalid credentials');
    }

    const token = jwt.sign({ userId: user.id }, 'your-secret-key', { expiresIn: '10h' });

    const userWithoutPassword = { ...user.toJSON() };
    delete userWithoutPassword.password;

    return { token, user: userWithoutPassword };
  }

  /**
   * Register user
   * @param {string} email User email
   * @param {string} password User password
   * @param {string} firstName User first name
   * @param {string} lastName User last name
   */
  async register(payload, transaction) {
    const user = await this.models.User.create(payload, transaction);
    return user;
  }

  /**
   * Get user
   * @param {string} id User id
   */
  async getUserById(UserId) {
    return this.models.User.findByPk({ where: { UserId } });
  }
}

module.exports = UsersService;

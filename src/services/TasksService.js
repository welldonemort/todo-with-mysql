/* eslint-disable class-methods-use-this */
const Models = require('../models/sequelize');

class TasksService {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  async inTransaction(work) {
    const t = await this.client.transaction();
    try {
      await work(t);
      return t.commit();
    } catch (error) {
      t.rollback();
      throw error;
    }
  }

  /**
   * Get task
   * @param {string} title Task title
   */
  async getTaskByTitle(title) {
    return this.models.Task.findOne({ where: { title } });
  }

  /**
   * Get list of users tasks
   * @param {string} UserId User ID
   * @param {boolean} done Status of a task
   */
  async getUserTasks(UserId, done = false) {
    return this.models.Task.findAll({ where: { UserId, done } });
  }

  /**
   * Create task for a user
   * @param {*} params Params of a task
   */
  async createTask(params, transaction) {
    const task = await this.models.Task.create(params, transaction);
    return task;
  }

  /**
   * Edit task
   * @param {string} title Title of a task
   * @param {*} params Params of a task
   */
  async updateTaskByTitle(title, params, transaction) {
    await this.models.Task.update(params, { where: { title } }, transaction);
    return this.models.Task.findOne({ where: { title } });
  }

  /**
   * Delete task
   * @param {string} taskTitle Title of a task
   */
  async deleteTaskByTitle(title, transaction) {
    const deletedRowsCount = await this.models.Task.destroy({ where: { title } }, transaction);
    return deletedRowsCount;
  }
}

module.exports = TasksService;
